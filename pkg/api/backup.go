package api

import (
	"context"
	"encoding/json"
	"fmt"

	"net/http"
	"time"

	"github.com/dustin/go-humanize"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	log "github.com/sirupsen/logrus"

	"github.com/stefanprodan/mgob/pkg/backup"
	"github.com/stefanprodan/mgob/pkg/config"
	"github.com/stefanprodan/mgob/pkg/db"
	"github.com/stefanprodan/mgob/pkg/notifier"
)

func configCtx(data config.AppConfig) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			r = r.WithContext(context.WithValue(r.Context(), "app.config", data))
			next.ServeHTTP(w, r)
		})
	}
}

func (s *HttpServer) postBackup(w http.ResponseWriter, r *http.Request) {
	status := "200"
	var backupLog string
	cfg := r.Context().Value("app.config").(config.AppConfig)
	planID := chi.URLParam(r, "planID")
	plan, err := config.LoadPlan(cfg.ConfigPath, planID)
	if err != nil {
		render.Status(r, http.StatusInternalServerError)
		render.JSON(w, r, map[string]string{"error": err.Error()})
		return
	}

	result := &config.Scheduler{}

	err = json.NewDecoder(r.Body).Decode(&result)
	if err != nil {
		render.Status(r, http.StatusInternalServerError)
		render.JSON(w, r, map[string]string{"error": err.Error()})
		return
	}

	plan.Scheduler = result

	log.WithField("plan", planID).Info("On demand backup started")

	res, err := backup.Run(plan, &cfg)
	if err != nil {
		status = "500"
		backupLog = fmt.Sprintf("Backup failed %v", err)
		log.WithField("plan", planID).Errorf("On demand backup failed %v", err)
		if err := notifier.SendNotification(fmt.Sprintf("%v on demand backup failed", planID),
			err.Error(), true, plan); err != nil {
			log.WithField("plan", plan.Name).Errorf("Notifier failed for on demand backup %v", err)
		}
		render.Status(r, http.StatusInternalServerError)
		render.JSON(w, r, map[string]string{"error": err.Error()})
	} else {
		log.WithField("plan", plan.Name).Infof("On demand backup finished in %v archive %v size %v",
			res.Duration, res.Name, humanize.Bytes(uint64(res.Size)))
		if err := notifier.SendNotification(fmt.Sprintf("%v on demand backup finished", plan.Name),
			fmt.Sprintf("%v backup finished in %v archive size %v",
				res.Name, res.Duration, humanize.Bytes(uint64(res.Size))),
			false, plan); err != nil {
			log.WithField("plan", plan.Name).Errorf("Notifier failed for on demand backup %v", err)
		}
		render.JSON(w, r, toBackupResult(res))
	}

	st := &db.Status{
		LastRun:       &res.Timestamp,
		LastRunStatus: status,
		Plan:          plan.Name,
		LastRunLog:    backupLog,
	}

	if err := s.Stats.Put(st); err != nil {
		log.WithField("plan", plan.Name).Errorf("Status store failed %v", err)
	}
}

type backupResult struct {
	Plan         string    `json:"plan"`
	File         string    `json:"file"`
	Duration     string    `json:"duration"`
	Size         int64     `json:"size"`
	SizeHumanize string    `json:"sizeHumanize"`
	Timestamp    time.Time `json:"timestamp"`
}

func toBackupResult(res backup.Result) backupResult {
	return backupResult{
		Plan:         res.Plan,
		Duration:     res.Duration.String(),
		File:         res.Name,
		Size:         res.Size,
		SizeHumanize: humanize.Bytes(uint64(res.Size)),
		Timestamp:    res.Timestamp,
	}
}
